<?php
/**
 * Created by PhpStorm.
 * User: George Novik
 * Company: Skyeng
 * Date: 19.11.2019
 * Time: 01:55
 */

namespace App\API;


use App\Currency\GBP;
use App\Currency\USD;
use App\DTO\Request\ItemDTO;
use App\DTO\Response\BasketCalculationResultDTO;
use App\Model\Basket;
use App\Model\Item;
use App\Service\CurrencyConverter;
use App\Service\DiscountService;

class BasketApi
{

    /**
     * @var DiscountService
     */
    protected $discountService;

    /**
     * @var CurrencyConverter
     */
    protected $currencyConverter;

    /**
     * BasketApi constructor.
     * @param DiscountService $discountService
     * @param CurrencyConverter $currencyConverter
     */
    public function __construct(
        DiscountService $discountService,
        CurrencyConverter $currencyConverter
    ) {
        $this->discountService = $discountService;
        $this->currencyConverter = $currencyConverter;
    }


    /**
     * @param ItemDTO[] $dtoItems
     */
    public function calculate(array $dtoItems) {

        $basket = new Basket();
        foreach ($dtoItems as $dtoItem) {

            $amount = GBP::createFromReal($dtoItem->getPrice());

            $item = new Item(
                $dtoItem->getId(),
                $dtoItem->getDescription(),
                $amount,
                $dtoItem->getQuantity()
            );

            $basket->addItem($item);
        }

        $discount = $this->discountService->selectDiscount($basket);
        $basket->applyDiscount($discount);

        $total = $basket->getTotal();

        $totalInUSD = $this->currencyConverter->convert($total, USD::class);

        return new BasketCalculationResultDTO([
            'total' => $totalInUSD->toRealString(),
            'currency' => $totalInUSD->getCurrency(),
            'discountApplied' => $discount
        ]);
    }

}