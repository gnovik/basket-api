<?php
/**
 * Created by PhpStorm.
 * User: George Novik
 * Company: Skyeng
 * Date: 19.11.2019
 * Time: 00:51
 */

namespace App\Controller;


use App\API\BasketApi;
use App\DTO\Response\BasketCalculationResultDTO;
use JMS\Serializer\Exception\Exception as SerializerException;
use JMS\Serializer\SerializerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\ConstraintViolationListNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BasketController
{
    /**
     * @var BasketApi
     */
    protected $basketApi;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * BasketController constructor.
     * @param BasketApi $basketApi
     * @param LoggerInterface $logger
     * @param ValidatorInterface $validator
     * @param SerializerInterface $serializer
     */
    public function __construct(
        BasketApi $basketApi,
        LoggerInterface $logger,
        ValidatorInterface $validator,
        SerializerInterface $serializer
    ) {
        $this->basketApi = $basketApi;
        $this->logger = $logger;
        $this->validator = $validator;
        $this->serializer = $serializer;
    }


    /**
     * @param Request $request
     * @Route("/basket/calculate", methods={"POST"})
     */
    public function calculateAction(Request $request)
    {
        try {
            $input = $request->getContent();

            try {
                $items = $this->serializer->deserialize(
                    $input,
                    'array<App\DTO\Request\ItemDTO>',
                    'json'
                );
            } catch (SerializerException $e) {
                $this->logger->info(
                    'Serialization problem',
                    [
                        'exception' => $e,
                    ]
                );

                return new JsonResponse('', 400);
            }

            $violations = $this->validator->validate(
                $items,
                [
                    new Assert\NotNull(),
                    new Assert\Valid(),
                    new Assert\All(
                        [
                            new Assert\Type("App\DTO\Request\ItemDTO"),
                        ]
                    ),
                ]
            );

            if ($violations->count() > 0) {
                $this->logger->info(
                    'Validation problem',
                    [
                        'violations' => $violations,
                    ]
                );

                return new JsonResponse('', 400);
            }

            $result = $this->basketApi->calculate($items);

            return JsonResponse::fromJsonString(
                $this->serializer->serialize($result, 'json')
            );
        } catch (\Throwable $e) {
            $this->logger->warning(
                'Server problem',
                [
                    'exception' => $e,
                ]
            );

            return new JsonResponse('', 500);
        }
    }
}