<?php

namespace App\DTO\Request;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;

/**
 * Class representing the Item model.
 */
class ItemDTO
{
    /**
     * @var int|null
     * @SerializedName("id")
     * @Assert\Type("int")
     * @Assert\NotNull
     * @Type("int")
     */
    protected $id;

    /**
     * @var string|null
     * @SerializedName("description")
     * @Assert\Type("string")
     * @Type("string")
     */
    protected $description;

    /**
     * Price in GBP
     *
     * @var string|null
     * @SerializedName("price")
     * @Assert\Type("string")
     * @Assert\NotNull
     * @Type("string")
     */
    protected $price;

    /**
     * @var int|null
     * @SerializedName("quantity")
     * @Assert\Type("int")
     * @Assert\NotNull
     * @Type("int")
     */
    protected $quantity;

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->id = isset($data['id']) ? $data['id'] : null;
        $this->description = isset($data['description']) ? $data['description'] : null;
        $this->price = isset($data['price']) ? $data['price'] : null;
        $this->quantity = isset($data['quantity']) ? $data['quantity'] : null;
    }

    /**
     * Gets id.
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets id.
     *
     * @param int|null $id
     *
     * @return $this
     */
    public function setId($id = null)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Gets description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets description.
     *
     * @param string|null $description
     *
     * @return $this
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Gets price.
     *
     * @return string|null
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Sets price.
     *
     * @param float|null $price  Price in GBP
     *
     * @return $this
     */
    public function setPrice($price = null)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Gets quantity.
     *
     * @return int|null
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Sets quantity.
     *
     * @param int|null $quantity
     *
     * @return $this
     */
    public function setQuantity($quantity = null)
    {
        $this->quantity = $quantity;

        return $this;
    }
}


