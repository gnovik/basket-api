<?php

namespace App\DTO\Response;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;

/**
 * Class representing the CartCalculationResult model.
 */
class BasketCalculationResultDTO
{
    /**
     * @var string|null
     * @SerializedName("total")
     * @Assert\Type("string")
     * @Type("string")
     */
    protected $total;

    /**
     * @var string|null
     * @SerializedName("currency")
     * @Assert\Type("string")
     * @Type("string")
     */
    protected $currency;

    /**
     * Percent
     *
     * @var float|null
     * @SerializedName("discountApplied")
     * @Assert\Type("float")
     * @Type("float")
     */
    protected $discountApplied;

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->total = isset($data['total']) ? $data['total'] : null;
        $this->currency = isset($data['currency']) ? $data['currency'] : null;
        $this->discountApplied = isset($data['discountApplied']) ? $data['discountApplied'] : null;
    }

    /**
     * Gets total.
     *
     * @return string|null
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Sets total.
     *
     * @param string|null $total
     *
     * @return $this
     */
    public function setTotal($total = null)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Gets currency.
     *
     * @return string|null
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Sets currency.
     *
     * @param string|null $currency
     *
     * @return $this
     */
    public function setCurrency($currency = null)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Gets discountApplied.
     *
     * @return float|null
     */
    public function getDiscountApplied()
    {
        return $this->discountApplied;
    }

    /**
     * Sets discountApplied.
     *
     * @param float|null $discountApplied  Percent
     *
     * @return $this
     */
    public function setDiscountApplied($discountApplied = null)
    {
        $this->discountApplied = $discountApplied;

        return $this;
    }
}


