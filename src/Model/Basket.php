<?php
/**
 * Created by PhpStorm.
 * User: George Novik
 * Company: Skyeng
 * Date: 19.11.2019
 * Time: 01:59
 */

namespace App\Model;


use App\Currency\GBP;

class Basket
{
    /**
     * @var Item[]
     */
    protected $items = [];

    /**
     * @var float
     */
    protected $discount = 0;


    public function addItem(Item $item) {
        $this->items[] = $item;
    }

    public function applyDiscount(float $percent) {
        $this->discount = $percent;
    }

    public function getTotal() {
        $total = 0;

        foreach ($this->items as $item) {
            $total += $item->getPrice()->toCent() * $item->getQuantity();
        }

        if ($this->discount > 0) {
            $absoluteDiscount = bcmul($total, $this->discount);

            $total = bcsub($total, $absoluteDiscount);
        }

        $amount = GBP::createFromCent($total);

        return $amount;
    }
}