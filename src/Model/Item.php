<?php
/**
 * Created by PhpStorm.
 * User: George Novik
 * Company: Skyeng
 * Date: 19.11.2019
 * Time: 01:59
 */

namespace App\Model;


use App\Currency\Amount;
use App\Currency\GBP;

class Item
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $description = '';

    /**
     * Basket works with GBP only
     * @var GBP
     */
    protected $price;

    /**
     * @var int
     */
    protected $quantity;

    /**
     * Item constructor.
     * @param int $id
     * @param string $description
     * @param GBP $price
     * @param int $quantity
     */
    public function __construct(int $id, string $description, GBP $price, int $quantity = 1)
    {
        $this->id = $id;
        $this->description = $description;
        $this->price = $price;
        $this->quantity = $quantity;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return GBP
     */
    public function getPrice(): GBP
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }



}