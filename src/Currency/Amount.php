<?php
/**
 * Created by PhpStorm.
 * User: George Novik
 * Company: Skyeng
 * Date: 19.11.2019
 * Time: 02:30
 */

namespace App\Currency;

class Amount {

    /**
     * @var int
     */
    protected $value;

    /**
     * @var string
     */
    protected $currency;

    /**
     * Amount constructor.
     * @param int $value
     * @param string $currency
     */
    public function __construct($value, $currency)
    {
        $this->value = $value;
        $this->currency = $currency;
    }

    public function toRealString() {
        return bcdiv($this->value, 100, 2);
    }

    public function toCent() {
        return $this->value;
    }

    public static function createFromCent(int $valueInCent) {
        return new static($valueInCent, static::class);
    }

    public static function createFromReal(string $value) {
        $valueInCent = (int) \bcmul($value, 100, 0);

        return new static($valueInCent, static::class);
    }

    public function getCurrency() {
        return $this->currency;
    }

}