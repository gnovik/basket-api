<?php
/**
 * Created by PhpStorm.
 * User: George Novik
 * Company: Skyeng
 * Date: 19.11.2019
 * Time: 02:27
 */

namespace App\Currency;



class USD extends Amount
{


    public function __toString()
    {
        return 'USD';
    }

}