<?php
/**
 * Created by PhpStorm.
 * User: George Novik
 * Company: Skyeng
 * Date: 19.11.2019
 * Time: 03:04
 */

namespace App\Service\CurrencyConverter;


class MissingCourseException extends \Exception
{


    public static function throwForDirection($fromCurrency, $toCurrency) {
        $message = "Missing convert course for $fromCurrency - $toCurrency";

        throw new static($message);
    }

}