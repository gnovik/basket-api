<?php
/**
 * Created by PhpStorm.
 * User: George Novik
 * Company: Skyeng
 * Date: 19.11.2019
 * Time: 02:57
 */

namespace App\Service;


use App\Currency\Amount;
use App\Currency\GBP;
use App\Currency\USD;
use App\Service\CurrencyConverter\MissingCourseException;

class CurrencyConverter
{
    protected static $rates = [
        GBP::class => [
            USD::class => 1.3
        ],
        USD::class => [
            GBP::class => 0.77
        ]
    ];

    /**
     * @param Amount $amount
     * @param string $toCurrencyClass
     * @return Amount
     */
    public function convert(Amount $amount, string $toCurrencyClass) {
        $fromCurrencyClass = get_class($amount);

        $course = $this->getConvertCourse($fromCurrencyClass, $toCurrencyClass);


        $value = bcmul($amount->toCent(), $course);

        return new $toCurrencyClass($value, $toCurrencyClass);

    }

    public function getConvertCourse($fromCurrencyClass, $toCurrencyClass) {
        if (false === array_key_exists($fromCurrencyClass, static::$rates)) {
            MissingCourseException::throwForDirection(
                $fromCurrencyClass,
                $toCurrencyClass
            );
        }

        $availableDirections = static::$rates[$fromCurrencyClass];

        if (false === array_key_exists($toCurrencyClass, $availableDirections)) {
            MissingCourseException::throwForDirection(
                $fromCurrencyClass,
                $toCurrencyClass
            );
        }

        return $availableDirections[$toCurrencyClass];
    }
}